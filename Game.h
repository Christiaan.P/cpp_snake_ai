#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <stdlib.h>

//Q libraries
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QTimer>
#include <QObject>

//Header files
#include <head.h>
#include <box.h>
#include <items.h>
#include <food.h>
#include <enemy.h>
#include <body.h>
#include <player.h>



class Game : public QObject
{
    Q_OBJECT
    public:
        QTimer * clock = new QTimer();
        Game();
        virtual ~Game();

    public slots:
        void play_game();

    private:
        QGraphicsScene *scene;
        Player * player;
        Box * box;
        Items * food;
        Items * enemy;
        Body * body;
        Head * head;

        qreal head_pos_x;
        qreal head_pos_y;
        qreal prev_pos_x;
        qreal prev_pos_y;
        short int score = 0;
        short int enemy_chance;

        bool death;
        bool grow;
        bool shorten;

};

#endif // GAME_H
