#ifndef FOOD_H
#define FOOD_H

#include <items.h>

class Food : public Items
{
    public:
        Food();
        bool eaten( qreal pos_x, qreal pos_y );
        void pos_switch();

    private:
        short int items_x;
        short int items_y;
};

#endif // FOOD_H
