#include <Game.h> 

Game::Game()
{
    scene = new QGraphicsScene();

    box = new Box();
    food = new Food();
    enemy = new Enemy();
    body = new Body();
    player = new Player();
    head = new Head( player );

    scene->addItem( box );

    scene->addItem( food );

    scene->addItem( enemy );

    scene->addItem( body );

    scene->addItem( player );
    player->setFlag( QGraphicsItem::ItemIsFocusable );
    player->setFocus();

    scene->addItem( head );

    connect( clock, SIGNAL( timeout() ), this, SLOT( play_game() ) );
    clock->start(50);

    QGraphicsView *view = new QGraphicsView( scene );
    view->show();
}

Game::~Game()
{
    delete scene;
    delete box;
    delete food;
    delete enemy;
    delete body;
    delete player;
    delete head;
}

void Game::play_game()
{
    grow = false;
    shorten = false;
    enemy_chance = rand() % 3;

    prev_pos_x = head_pos_x;
    prev_pos_y = head_pos_y;

    if( player->get_direction() == 'l' )
    {
        head_pos_x -= 15;
    }
    else if( player->get_direction() == 'r' )
    {
        head_pos_x += 15;
    }
    else if( player->get_direction() == 'u' )
    {
        head_pos_y -= 15;
    }
    else if( player->get_direction() == 'd' )
    {
        head_pos_y += 15;
    }

    if( food->eaten( head_pos_x, head_pos_y ) == true )
    {
        score++;
        std::cout << "Yum yum!!" << std::endl;
        food->pos_switch();
        grow = true;
        if( enemy_chance == 1 )
        {
            enemy->pos_switch();
            enemy->setVisible( true );
        }
        else
        {
            enemy->setVisible( false );
        }
    }

    if( enemy->eaten( head_pos_x, head_pos_y ) == false )
    {
        score--;
        std::cout << "Yikes! it's an enemy." << std::endl;
        enemy->setVisible( false );
        shorten = true;
    }

    if( box->check_Collision( head_pos_x, head_pos_y ) == true )
    {
        death = true;
    }

    if( death == true )
        {
            head_pos_x = 330;
            head_pos_y = 330;
            std::cout << "You died!" << std::endl;
            std::cout << "score: " << score << std::endl << std::endl;
            player->stop();
            food->pos_switch();
            body->delete_body( score );
            score = 0;
            death = false;
        }

    head->setPos( head_pos_x, head_pos_y );

    if( shorten && score >= 1 )
        {
            body->popLast();
        }
        else if( grow )
        {
            body = new Body( prev_pos_x, prev_pos_y, body );
            scene->addItem( body );
        }
        else
        {
            body->setPosition( prev_pos_x, prev_pos_y );
        }

    player->resetNewMove();
}
