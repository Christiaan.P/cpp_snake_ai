#ifndef ENEMY_H
#define ENEMY_H

#include <items.h>

class Enemy : public Items
{
public:
    Enemy();
    ~Enemy();
    bool eaten( qreal pos_x, qreal pos_y );
    void pos_switch();

private:
    short int items_x;
    short int items_y;
};

#endif // ENEMY_H
