TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += qt
QT += widgets
QT += gui

SOURCES += \
        Game.cpp \
        body.cpp \
        box.cpp \
        enemy.cpp \
        food.cpp \
        head.cpp \
        items.cpp \
        main.cpp \
        player.cpp

HEADERS += \
    Game.h \
    body.h \
    box.h \
    enemy.h \
    food.h \
    head.h \
    items.h \
    player.h
