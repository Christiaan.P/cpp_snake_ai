#ifndef ITEMS_H
#define ITEMS_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QBrush>
#include <stdlib.h>
#include <iostream>

class Items : public QObject, public QGraphicsRectItem
{
    Q_OBJECT

    private:
        short int items_x;
        short int items_y;
    public:
        Items();
        virtual bool eaten( qreal pos_x, qreal pos_y );
        virtual void pos_switch();
};

#endif // ITEMS_H
