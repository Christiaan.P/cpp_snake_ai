#include <player.h>

Player::Player()
{
    newMove = false;
    direction = 's';
}

bool Player::getNewMove() const
{
    return newMove;
}

void Player::resetNewMove()
{
    newMove = false;
}


void Player::keyPressEvent( QKeyEvent *event )
{
    if( !newMove )
    {
        if( event->key() == Qt::Key_Left && direction != 'r' )
        {
            direction = 'l';
            newMove = true;
        }
        else if( event->key() == Qt::Key_Right && direction != 'l' )
        {
            direction = 'r';
            newMove = true;
        }
        else if( event->key() == Qt::Key_Up && direction != 'd' )
        {
            direction = 'u';
            newMove = true;
        }
        else if( event->key() == Qt::Key_Down && direction != 'u' )
        {
            direction = 'd';
            newMove = true;
        }
    }
}

char Player::get_direction()
{
    return direction;
}

void Player::stop()
{
    direction = 's';
}
