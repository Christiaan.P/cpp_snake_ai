#ifndef HEAD_H
#define HEAD_H

#include <QGraphicsRectItem>
#include <QObject>
#include <player.h>
#include <QTimer>
#include <QBrush>
#include <box.h>
#include <items.h>
#include <food.h>
#include <enemy.h>
#include <body.h>
#include <stdlib.h>
#include <QGraphicsScene>


class Head : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
    public:
        Head( Player * player );
        void set_position();

    private:
        Player *player;
        qreal pos_x;
        qreal pos_y;
};

#endif // HEAD_H
