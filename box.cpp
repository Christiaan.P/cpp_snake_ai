#include <box.h>

Box::Box()
{
    setRect( 0, 0, 675, 675 );
    setBrush( Qt::lightGray );
}

bool Box::check_Collision( qreal pos_x, qreal pos_y )
{
    if( pos_x < 0 || pos_x > 660 )
    {
        return true;
    }
    else if( pos_y < 0 || pos_y > 660 )
    {
        return true;
    }
    else
    {
        return false;
    }
}
